part of views;

class EuclidPainter extends CustomPainter {
  EuclidPainter({
    @required this.pointsList,
    @required this.dotStyle,
    @required this.lineStyle,
    this.showGrid,
    this.closeShape,
  });

  List<Offset> pointsList;
  List<Offset> filteredPointsList = List();
  Paint dotStyle;
  Paint lineStyle;
  bool showGrid;
  bool closeShape;

  @override
  void paint(Canvas canvas, Size size) {
    if (showGrid) {
      buildGrid(canvas, size);
    } else if (pointsList.isEmpty) {
      canvas.save();
      canvas.restore();
    }

    if (pointsList.isEmpty) {
      return;
    }

    int numPoints = pointsList.length;

    for (int i = 0; i < numPoints - 1; i++) {
      Offset coord = pointsList[i];

      if (coord != null) {
        canvas.drawCircle(coord, 10, dotStyle);

        if (i > 1 && i < numPoints - 1) {
          Offset prevCoord = pointsList[i - 2];
          canvas.drawLine(
            prevCoord,
            coord,
            lineStyle,
          );
        }
        if (closeShape) {
          canvas.drawLine(
            pointsList[0],
            pointsList[numPoints - 2],
            lineStyle,
          );
        }
      }
    }
  }

  @override
  bool shouldRepaint(EuclidPainter oldDelegate) => true;

  void buildGrid(Canvas canvas, Size size) {
    Paint gridStyle = Paint()
      ..color = gridColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;

    Path grid = Path();
    for (var i = 1; i <= 4; i++) {
      grid.moveTo(size.width * i * 0.2, 0);
      grid.lineTo(size.width * i * 0.2, size.height);
    }

    for (var i = 1; i <= 10; i++) {
      grid.moveTo(0, size.height * i * 0.1);
      grid.lineTo(size.width, size.height * i * 0.1);
    }

    canvas.drawPath(grid, gridStyle);
  }
}
