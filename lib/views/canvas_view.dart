part of views;

class CanvasView extends StatefulWidget {
  const CanvasView({Key key}) : super(key: key);

  @override
  _CanvasViewState createState() => _CanvasViewState();
}

class _CanvasViewState extends State<CanvasView> {
  GlobalKey<ScaffoldState> scaffold = GlobalKey<ScaffoldState>();
  OverlayEntry overlayEntry;

  Color pickerColor = lineColor;
  double strokeWidth = 6;
  List<Offset> points = [];

  Paint dotStyle = Paint()
    ..isAntiAlias = true
    ..color = dotColor
    ..strokeWidth = 5;

  Paint lineStyle;
  bool showGrid = true;
  bool closeShape = false;

  @override
  void initState() {
    super.initState();

    lineStyle = Paint()
      ..strokeCap = StrokeCap.round
      ..isAntiAlias = true
      ..color = lineColor
      ..strokeWidth = strokeWidth;
  }

  OverlayEntry createOverlayEntry(Size size) => OverlayEntry(
        builder: (context) => StrokeToolbar(
          size: size,
          offset: 32,
          strokeWidth: strokeWidth,
          closeToolbar: () {
            if (overlayEntry != null) {
              overlayEntry.remove();
              overlayEntry = null;
            }
          },
          onSelectColor: (Color color) {
            onChoseColor(color);
            useColor();
          },
          onChangeStroke: (double value) {
            strokeWidth = value;
            lineStyle.strokeWidth = value;
            setState(() {});
          },
        ),
      );

  void onPanStart(DragStartDetails details) {
    RenderBox renderBox = context.findRenderObject();
    points.add(renderBox.globalToLocal(details.globalPosition));
    if (closeShape) {
      closeShape = false;
    }
    setState(() {});
  }

  void onPanEnd(DragEndDetails details) {
    points.add(null);

    setState(() {});
  }

  void clearCanvas() {
    points = [];
    setState(() {});
  }

  void onCloseShape() {
    print(points.length);
    if (points.length < 6 && scaffold.currentState != null) {
      scaffold.currentState.showSnackBar(
        SnackBar(
          backgroundColor: gold,
          content:
              Text('To close the polygon a minimun of 3 dots are necessary'),
        ),
      );

      return;
    }
    closeShape = true;
    setState(() {});
  }

  void showStrokeWidthTool(BuildContext ctx) {
    if (overlayEntry != null) {
      overlayEntry.remove();
      overlayEntry = null;
      return;
    }

    OverlayState overlayState = Overlay.of(ctx);
    Size size = MediaQuery.of(ctx).size;

    overlayEntry = createOverlayEntry(size);
    overlayState.insert(overlayEntry);
  }

  void onChoseColor(Color color) {
    pickerColor = color;
  }

  void useColor() {
    lineStyle.color = pickerColor;
    setState(() {});
  }

  void choseColor() {
    showDialog(
      context: context,
      child: ColorPickerModal(
        lastColorChosen: pickerColor,
        onChoseColor: onChoseColor,
        onSaveColor: useColor,
      ),
    );
  }

  void toggleGrid() {
    showGrid = !showGrid;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) => AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Scaffold(
          key: scaffold,
          body: GestureDetector(
            onPanStart: onPanStart,
            onPanEnd: onPanEnd,
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: CustomPaint(
                size: Size.infinite,
                painter: EuclidPainter(
                  pointsList: points,
                  dotStyle: dotStyle,
                  lineStyle: lineStyle,
                  showGrid: showGrid,
                  closeShape: closeShape,
                ),
              ),
            ),
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.miniEndDocked,
          floatingActionButton: FloatingActionButton(
            onPressed: onCloseShape,
            tooltip: 'Close shape',
            child: SvgPicture.asset(
              'assets/icons/close_shape.svg',
              color: textColor,
              width: UILayout.large,
              height: UILayout.large,
              fit: BoxFit.contain,
            ),
          ),
          bottomNavigationBar: BottomToolbar(
            choseColor: choseColor,
            choseStrokeWidth: () {
              showStrokeWidthTool(context);
            },
            toggleGrid: toggleGrid,
            clearCanvas: clearCanvas,
          ),
        ),
      );
}
