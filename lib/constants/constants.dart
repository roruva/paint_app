library constants;

/// [dart-packages]
import 'package:flutter/material.dart';

/// [lib-components]
part './colors.dart';
part './ui_layout.dart';
