part of constants;

Map<int, Color> primaryColorValues = {
  50: Color(0xFFE5E5E5),
  100: Color(0xFFBFBFBF),
  200: Color(0xFF949494),
  300: Color(0xFF696969),
  400: Color(0xFF484848),
  500: Color(0xFF282828),
  600: Color(0xFF242424),
  700: Color(0xFF1E1E1E),
  800: Color(0xFF181818),
  900: Color(0xFF0F0F0F),
};

MaterialColor primaryColor = MaterialColor(0xFF282828, primaryColorValues);
Color gold = Color.fromRGBO(246, 188, 45, 1);

// Paragraph color
Color textColor = Colors.white.withOpacity(0.7);

// Border
Color borderColor = Colors.white.withOpacity(0.3);
Color lineColor = Colors.black.withOpacity(0.7);

// Polygon
Color dotColor = Colors.redAccent.withOpacity(0.7);

// Grid
Color gridColor = Colors.grey.withOpacity(0.1);
