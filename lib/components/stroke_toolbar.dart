part of components;

class StrokeToolbar extends StatelessWidget {
  const StrokeToolbar({
    this.size,
    this.offset,
    this.strokeWidth,
    this.closeToolbar,
    this.onSelectColor,
    this.onChangeStroke,
    this.lastColors = const <Color>[
      Colors.blue,
      Colors.green,
      Colors.purple,
      Colors.pink,
      Colors.amber
    ],
    Key key,
  }) : super(key: key);

  final Size size;
  final double offset;
  final double strokeWidth;
  final List<Color> lastColors;
  final Function() closeToolbar;
  final Function(Color color) onSelectColor;
  final Function(double value) onChangeStroke;

  @override
  Widget build(BuildContext context) => Positioned(
        left: offset / 2,
        bottom: 100,
        width: size.width - offset,
        child: DecoratedBox(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(24),
            border: Border.all(
              color: textColor,
              width: 1,
            ),
            color: primaryColor[500],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(16),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      child: Icon(
                        Icons.close,
                        color: textColor,
                      ),
                      onTap: closeToolbar,
                    ),
                  ),
                ),
              ),
              RichText(
                text: TextSpan(
                  text: '${strokeWidth.toInt()} ',
                  style: TextStyle(
                    fontSize: 24,
                    color: textColor,
                    fontWeight: FontWeight.bold,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'pts',
                      style: TextStyle(
                        fontSize: 16,
                        color: textColor,
                        fontWeight: FontWeight.normal,
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(
                  UILayout.medium,
                  0,
                  UILayout.medium,
                  UILayout.mlarge,
                ),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Material(
                        color: Colors.transparent,
                        child: Slider(
                          value: strokeWidth,
                          max: 12,
                          min: 4,
                          divisions: 8,
                          label: '$strokeWidth',
                          activeColor: textColor,
                          inactiveColor: primaryColor[400],
                          onChanged: onChangeStroke,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 1,
                child: ColoredBox(
                  color: borderColor,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: UILayout.medium,
                  vertical: UILayout.mlarge,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    ...lastColors.map(
                      (Color color) => Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            onSelectColor(color);
                          },
                          child: DecoratedBox(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                bottomRight: Radius.circular(20),
                              ),
                              border: Border.all(
                                color: textColor,
                                width: 1,
                              ),
                              color: color,
                            ),
                            child: SizedBox(
                              width: 40,
                              height: 40,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
}
