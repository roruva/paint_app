part of components;

class ColorPickerModal extends StatelessWidget {
  const ColorPickerModal({
    this.lastColorChosen,
    this.onChoseColor,
    this.onSaveColor,
    Key key,
  }) : super(key: key);

  final Color lastColorChosen;
  final void Function(Color color) onChoseColor;
  final void Function() onSaveColor;

  @override
  Widget build(BuildContext context) => AlertDialog(
        title: Text('Pick a color!'),
        content: SingleChildScrollView(
          child: ColorPicker(
            pickerColor: lastColorChosen,
            onColorChanged: onChoseColor,
            pickerAreaHeightPercent: 0.8,
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Save'),
            color: primaryColor[500],
            onPressed: () {
              onSaveColor();
              Navigator.of(context).pop();
            },
          ),
        ],
      );
}
