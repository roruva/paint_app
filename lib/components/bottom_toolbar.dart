part of components;

class BottomToolbar extends StatelessWidget {
  const BottomToolbar({
    @required this.choseColor,
    @required this.choseStrokeWidth,
    @required this.toggleGrid,
    @required this.clearCanvas,
    Key key,
  }) : super(key: key);

  final void Function() choseColor;
  final void Function() choseStrokeWidth;
  final void Function() toggleGrid;
  final void Function() clearCanvas;

  @override
  Widget build(BuildContext context) => BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        color: primaryColor[500],
        child: Padding(
          padding: EdgeInsets.fromLTRB(
            0,
            UILayout.small,
            UILayout.xlarge,
            UILayout.small,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Tooltip(
                message: 'Clean canvas',
                child: IconButton(
                  icon: Icon(
                    Icons.delete_outline,
                    color: textColor,
                  ),
                  onPressed: clearCanvas,
                ),
              ),
              Tooltip(
                message: 'Toggle grid',
                child: IconButton(
                  icon: SvgPicture.asset(
                    'assets/icons/grid_4x4.svg',
                    color: textColor,
                    width: UILayout.large,
                    height: UILayout.large,
                    fit: BoxFit.contain,
                  ),
                  onPressed: toggleGrid,
                ),
              ),
              Tooltip(
                message: 'Change the stroke width of the line',
                child: IconButton(
                  icon: Icon(
                    Icons.edit_outlined,
                    color: textColor,
                  ),
                  onPressed: choseStrokeWidth,
                ),
              ),
              Tooltip(
                message: 'Change the line color',
                child: IconButton(
                  icon: Icon(
                    Icons.color_lens_outlined,
                    color: textColor,
                  ),
                  onPressed: choseColor,
                ),
              ),
            ],
          ),
        ),
      );
}
