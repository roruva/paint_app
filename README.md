# paint_app

App to draw polygons with points.

## Screenshots
<div style="display:flex;flex-direction:row;">
  <img src="screenshots/flutter_01.jpg" alt="Paint App" height="400" style="margin-right: 5px"/>
  <img src="screenshots/flutter_02.jpg" alt="Paint App" height="400" style="margin-right: 5px"/>
</div>

<div style="display:flex;flex-direction:row;">
  <img src="screenshots/flutter_03.jpg" alt="Paint App" height="400" style="margin-right: 5px"/>
  <img src="screenshots/flutter_04.jpg" alt="Paint App" height="400" style="margin-right: 5px"/>
</div>

### Run project
First of all download the dependencies:
```sh
flutter pub get
```
Then run on devices or simulator
```sh
flutter run
```

### Specs
These were the specifications used to build the project:\
\
**Flutter:** 1.22.0 \
**OS:** Mac OS X 10.15.5\
**dart:** 2.7.2